package com.gms.widget.layout.component;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.SeekBar;

import com.gms.widget.layout.R;

import androidx.appcompat.widget.AppCompatSeekBar;

/**
 * Custom SeekBar
 * 步數組件
 */
public class CustomSeekBar extends AppCompatSeekBar {

    private static final int ANIMATION_DURATION = 500;

    static final boolean IS_USE_INIT_PROGRESS_ANIMATION = false;

    public CustomSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init() {

        this.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                doShowThumb();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getWidth() > 0) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    doShowThumb();
                }
            }
        });
    }

    // 處理Thumb的顯示
    private void doShowThumb() {
        float paddingRight = getResources().getDimension(R.dimen.custom_seekbar_thumb_padding);
        if (getProgress() * 0.01f * getWidth() > paddingRight) {
            getThumb().setAlpha(255);
        } else {
            getThumb().setAlpha(0);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    @Override
    public synchronized void setProgress(final int progress) {

        if (!IS_USE_INIT_PROGRESS_ANIMATION) {
            super.setProgress(progress);
            return;
        }

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(getProgress(), progress);
        valueAnimator.setDuration(ANIMATION_DURATION);
        valueAnimator.start();
        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                CustomSeekBar.super.setProgress((int) animation.getAnimatedValue());
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                valueAnimator.removeAllListeners();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

    }
} // class close
