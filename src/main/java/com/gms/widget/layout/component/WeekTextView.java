package com.gms.widget.layout.component;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.gms.widget.layout.R;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * 0442_聽障者健康紀錄
 * 日期元件
 */
public class WeekTextView extends AppCompatTextView {

    public @interface Status {
        /**
         * 空
         */
        int NON = -1;
        /**
         * 未達成目標
         */
        int SAD = 0;
        /**
         * 達成目標
         */
        int SMILE = 1;
    }

    public WeekTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeekTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init() {
        setStatus(Status.NON);
    }

    /**
     * 設定狀態
     *
     * @param status
     */
    public void setStatus(@Status int status) {
        Drawable drawable = null;
        switch (status) {
            case Status.SAD:
                drawable = getResources().getDrawable(R.drawable.ic_icon_sad);
                break;
            case Status.SMILE:
                drawable = getResources().getDrawable(R.drawable.ic_icon_smile);
                break;
            default:
                drawable = null;
        }
        this.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
    }


} // class close

