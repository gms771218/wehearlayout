package com.gms.widget.layout.component

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.gms.widget.layout.R

open class AzulLineChart(context: Context?, attrs: AttributeSet?) : LineChart(context , attrs ) {

    // 資料集
    var mHeartDatas = ArrayList<Entry>()
    lateinit var lineDataSet: LineDataSet

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        initChartSetting()
    }

    open fun initChartSetting() {

        // 隱藏圖表說明
        this.legend.isEnabled = false
        // 設定邊界
        this.setViewPortOffsets(0f, 0f, 0f, 0f)
        this.isDoubleTapToZoomEnabled = false
        this.isDragEnabled = false
        this.setScaleEnabled(false)

        // 隱藏說明描述
        this.description.isEnabled = false

        // 不繪製右側標籤
        this.getAxis(YAxis.AxisDependency.RIGHT).isEnabled = false


        // 繪製左側標籤
        var yAxis: YAxis = this.getAxis(YAxis.AxisDependency.LEFT)
        yAxis.axisMinimum = 40f
        yAxis.axisMaximum = 200f
        yAxis.setLabelCount(5, true)
//        yAxis.textSize = 18f
//        yAxis.textColor = resources.getColor(R.color.azul_black_1)
        yAxis.gridColor = resources.getColor(R.color.azul_gray_5 )
        yAxis.setDrawLabels(false)

        // 繪製上方標籤
        var xAxis: XAxis = this.xAxis
        xAxis.axisMinimum = 0f
        xAxis.mAxisMaximum = 30f
        xAxis.setLabelCount(15, true)
        xAxis.setDrawLabels(false)
        xAxis.gridColor = resources.getColor(R.color.azul_gray_5)


        var emptyDataSet = LineDataSet(null, "")
        for (pos in 0..29) {
            emptyDataSet.addEntry(Entry(pos.toFloat(), 0f))
        }

        lineDataSet = LineDataSet(mHeartDatas, "")
        lineDataSet.color = resources.getColor(R.color.azul_red_1)
        lineDataSet.lineWidth = 2f
        lineDataSet.setCircleColor(resources.getColor(R.color.azul_red_1))
        lineDataSet.circleRadius = 1f
        lineDataSet.setDrawCircles(true)
        lineDataSet.setDrawValues(false)
        lineDataSet.setDrawHighlightIndicators(false)

        this.data = LineData(emptyDataSet, lineDataSet)

    }

    /**
     * 顯示資料
     */
    open fun showData(value: Float) {
        updateData(Entry(0f, value))
    }

    /**
     * 顯示資料
     */
    open fun showData(values: List<Float>) {
        var array = arrayListOf<Entry>()
        for (value in values)
            array.add(Entry(array.size.toFloat(), value))
        updateData(array)
    }

    open fun clearData(){
        mHeartDatas.clear()
        notifyDataSetChanged()
        postInvalidate()
    }

    private fun updateData(newData: Entry) {
        updateData(arrayOf(newData).toList())
    }

    private fun updateData(newDatas: List<Entry>) {

        mHeartDatas.map {
            it.x += newDatas.size
        }
        mHeartDatas.addAll(0, newDatas)

        mHeartDatas.takeIf { mHeartDatas.size >= 30 }?.run {

            mHeartDatas.subList(29, mHeartDatas.size - 1)?.clear()
        }
        notifyDataSetChanged()
        postInvalidate()

    }

}