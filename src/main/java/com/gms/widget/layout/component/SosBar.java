package com.gms.widget.layout.component;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.gms.widget.layout.R;

import androidx.annotation.FloatRange;
import androidx.customview.widget.ViewDragHelper;

/**
 * SOS bar
 */
public class SosBar extends FrameLayout {

    static int ANIMATION_DURATION = 500;

    ImageView imgView;

    IPositionListener mIListener;

    ViewDragHelper mDragHelper;


    public SosBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SosBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        imgView = new ImageView(getContext());
        FrameLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgView.setLayoutParams(params);
        imgView.setImageResource(R.drawable.ic_btn_sos);
        this.addView(imgView);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mDragHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child.equals(imgView);
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                super.onViewPositionChanged(changedView, left, top, dx, dy);
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {

                // 左邊界
                int paddingLeft = getPaddingLeft();
                if (left < paddingLeft) {
                    if (mIListener != null)
                        mIListener.onCurrentPos(0);
                    return paddingLeft;
                }

                int endPos = getEndPos(imgView);
                // 右邊界
                if (left > endPos) {
                    if (mIListener != null)
                        mIListener.onCurrentPos(1);
                    return endPos;
                }

                if (mIListener != null)
                    mIListener.onCurrentPos((left / (float) endPos));

                // 未達邊界
                return left;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                // 頂部
                int paddingTop = getPaddingTop();
                if (top < paddingTop) {
                    return paddingTop;
                }

                // 底部
                int endPos = getHeight() - child.getHeight() - getPaddingBottom();
                if (top > endPos) {
                    return endPos;
                }
                return top;
            }

        });

    }

    // 攔截Touch事件
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDragHelper.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mDragHelper.processTouchEvent(ev);
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final ValueAnimator valueAnimator = ValueAnimator.ofInt(imgView.getLeft(), getPaddingLeft());
            valueAnimator.setDuration(ANIMATION_DURATION);
            valueAnimator.start();
            valueAnimator.setInterpolator(new AccelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int pos = (int) animation.getAnimatedValue();
                    imgView.layout(pos, imgView.getTop(), pos + imgView.getWidth(), imgView.getTop() + imgView.getHeight());
                    int endPos = getEndPos(imgView);
                    if (mIListener != null)
                        mIListener.onCurrentPos((pos / (float) endPos));

                }
            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    valueAnimator.removeAllListeners();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            return false;
        }
        return true;
    }

    public void setPositionListener(IPositionListener listener) {
        mIListener = listener;
    }

    /**
     * 計算右邊界位置
     *
     * @param view 總寬度 - 物件的寬度 and 偏移量 ，就是最後可移動的位置
     * @return
     */
    private int getEndPos(View view) {
        return getWidth() - view.getWidth() - getPaddingRight();
    }

    public static interface IPositionListener {
        /**
         * 計算物件初始位置和終點位置的比例值
         *
         * @param position 位置(0...1)
         */
        void onCurrentPos(@FloatRange(from = 0, to = 1) float position);
    }

} // class close
