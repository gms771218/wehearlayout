package com.gms.widget.layout.component

import android.content.Context
import android.util.AttributeSet
import com.github.mikephil.charting.components.YAxis
import com.gms.widget.layout.R

/**
 * 攝氧圖
 */
class OxygenLineChart(context: Context?, attrs: AttributeSet?) : AzulLineChart(context , attrs) {

    override fun initChartSetting() {
        super.initChartSetting()
        // 繪製左側標籤
        var yAxis: YAxis = this.getAxis(YAxis.AxisDependency.LEFT)
        yAxis.axisMinimum = 70f
        yAxis.axisMaximum = 100f
        yAxis.setLabelCount(4, true)
        yAxis.gridColor = resources.getColor(R.color.azul_gray_5)
        yAxis.setDrawLabels(false)
    }

}